1:1 <Verba Nehemiae filii.>BED. in Esdram, tom. 2. Nehemias interpretatur <consolator Domini,>etc., usque ad quasi diruta ab hostibus Hierosolymorum moenia restaurant. <Verba Nehemiae.>ID., ibid. Hucusque verba Esdrae, etc., usque ad qui non blandimentis temporalibus enervari, sed adversitatibus exerceri, et ad requiem sempiternam gaudent praeparari. <In mense Casleu.>ID., ibid. Mensis Casleu ipse est quem nos Decembrem vocamus, apud Hebraeos nonus, apud nos ultimus. Cujus nomen quod spes ejus interpretatur, bene congruit votis ejus, qui ad ruinas sanctae civitatis intendebat erigendas. Primum enim bonae actionis fundamentum est, spem habere de auxilio Domini ad perficiendum quod cupimus. In hoc mense natus est Dominus, cujus nomine longe ante figurabatur, quod in eo mense verus Nehemias diu desideratus ad aedificationem Ecclesiae esset venturus. <Eram in Susis.>ID., ibid. Susis metropolis est Persarum, sed tanta firmitate, ut castrum esse videatur, et interpretatur equitatio vel revertens, quae fidelibus convenienter aptatur, eis maxime, qui de captivitate Jerusalem curam gerunt, id est, de salute eorum, qui aliquando per insidias diaboli de Ecclesia rapti poenitentes denuo per gratiam Dei sunt redacti. Tales enim sunt in revertente castro, id est, in robore mentis revocatae ab infimis ad desiderium patriae coelestis, et in equitatu sanctorum cordium, quae scilicet portant sessorem Deum. <Qui remanserunt.>ID., ibid. Patet litterae sensus, etc., usque ad indignos arcere debuerant, avaritiae, luxuriae caeterorumque vitiorum incendio perire.
1:2 .
1:3 .
1:4 <Flevi et luxi.>Si vir sanctus audiens destructa lignorum et lapidum aedificia recte lugebat, jejunabat, et orabat, diu sedens in tristitia, quanto magis in destructione et ruina animarum continuis luctibus, et orationibus est insistendum, ut miserante Deo ad pristinam erigantur sospitatem, qui in opprobrio religionis triumphante inimico jacebant diutina vitiorum sorde squalentes.
1:5 .
1:6 .
1:7 .
1:8 .
1:9 .
1:10 .
1:11 .
2:1 <Factum est autem.>BED. in Esdram. Qui scilicet est primus mensis anni, etc., usque ad suum intimare. Erat quidem princeps vinarius, regi poculum porrigebat, officium laetitiae foris agebat, sed interius lugebat: quia civitatem sanctam dirutam, etc. Unde: <Super flumina Babylonis illic sedimus,>etc. Psal. CXXVI.
2:2 .
2:3 .
2:4 .
2:5 <Si videtur,>etc. ID., ibid. Sicut per Cyrum primum Persarum regem Christus significatur, etc., usque ad de qua tota prophetae sententia plenissime, prout potui, in libro temporum edisserere curavi.
2:6 .
2:7 .
2:8 .
2:9 .
2:10 <Et audierunt.>Contristantur haeretici, etc., usque ad et eos, qui peccando erraverunt, poenitendo redire cognoscunt.
2:11 .
2:12 .
2:13 .
2:14 .
2:15 <Considerabam murum Jerusalem.>Diversa destructae urbis loca lustrando pervagatur; et singula quomodo debeant reparari, sollicite scrutatur. Doctorum quoque spiritualium est saepius noctu surgere, et solerti indagine statum Ecclesiae quiescentibus caeteris inspicere, ut vigilanter inquirant, quomodo ea quae vitiorum bellis sordidata vel dejecta sunt, corrigant et erigant. Murus autem Jerusalem dissipatus jacet, et conversatio fidelium terrenis et infirmis sordet affectibus. Portae sunt igni consumptae, cum hi qui aliis introitum vitae pandere debuerant, relicto veritatis magistro communi, cum caeteris ignavia torpent, et temporalibus curis inserviunt.
2:16 .
2:17 <Quia Jerusalem deserta est.>BED., ibid. Plana sunt haec et spirituali sensu congrua: quia doctores, imo omnes qui zelo Dei fervent, in afflictione maxima sunt, quandiu Jerusalem, id est visionem pacis, quam nobis Deus reliquit et commendavit, bellis dissensionum cernunt esse desertam: et portas virtutum, quas juxta Isaiam laudatio occupare debuerat, praevalentibus inferorum portis dejectas, et opprobrio habitas.
2:18 .
2:19 .
2:20 .
3:1 <Et surrexit Eliasib sacerdos.>BEDA in Esdram, tom. 2, lib. 3. Id est, pontifex temporis illius filius Joachim, qui post patrem suum Jesum filium Josedec, non parvo tempore pontificatum tenuit. Et recte restauratio civitatis a sacerdote magno et fratribus ejus coepit: ut qui gradu praecesserant ordinis, exemplum fierent aliis in bonis operibus. Et bene aedificantibus sacerdotibus adjungitur: <Et usque ad turrim centum cubitorum sanctificaverunt eam usque ad turrim Hananael.>Aedificant enim sacerdotes in centenario numero cubitorum, cum omnes quos erudiunt, amore et desiderio aeternorum incendunt. Nam centum, quae in computo digitorum de laeva transeunt in dexteram, coelestia bona figurant, quae comparatio terrenorum quasi dextera ad sinistram sunt. <Et aedificaverunt.>ID., ibid. Vetus translatio habet, etc., usque ad et in illa laventur. ID., ibid. Videtur juxta litteram; etc., usque ad sola internae retributionis intentione protendunt.  <Portam gregis.>Quae, scilicet, respicit Joppen et Diospolim, id est, Lyddam, vicinior mari inter cunctas vias Jerusalem; quae nunc porta David fertur appellari, et esse prima portarum ad occidentem montis Sion. Huic opinioni consentire videntur Verba dierum, ubi scriptum est de Manasse rege Juda: <Post hoc aedificavit murum extra civitatem David,>etc. II Par. 12. BED., ibid. Typice autem, sicut grex significat Domini fideles, etc., usque ad temporibus autem Ezechiae duplicatum fuisse murum civitatis Verba dierum sic ostendunt: <Aedificavit quoque agens industrie omnem murum,>etc. II Par. 23. Typice autem iste Sophonias vocem clamoris a porta piscium et ululatum audivit a secunda: quia ab hostibus utramque dejiciendam praecognovit. Fidem enim et opera doctorum per quae ab undis vitae corruptibilis caeteros erui, et in Ecclesiam oportebat induci, vidit diaboli insidiis terrae esse sternenda, id est, per appetitum terrenae voluptatis coelestibus gaudiis esse privandos. Bene autem ab utraque porta, prima, scilicet, et secunda, interiori et exteriori, vocem clamoris et ululatus audivit: quia exterius opera, et intus corda diabolo impugnante vidit subvertenda. Sed quia Dominus erigit elisos, Nehemias portam piscium post ruinam narrat instauratam: quia si aliqui praedicatorum peccando corruerint, non deerunt usque in finem saeculi, qui succedentes portas justitiae Domino adjuvante fidelibus bene vivendo et praedicando aperiant. <Ipsi texerunt.>ID., ibid. Hic versus etiam de caeteris portis, etc., usque ad qui plus periculi nobis laudando afferant, quam salutaris adminiculi a nobis videndo referant.
3:2 .
3:3 .
3:4 .
3:5 .
3:6 <Ad portam veterem.>De qua dicit Joannes: <Charissimi, non mandatum novum scribo vobis, sed vetus quod habuistis ab initio>I Joan. 2.. <Mandatum vetus est verbum quod audistis.>Porta vero vetus aedificatur in Jerusalem, cum verbum fidei et dilectionis quod ab initio Ecclesiae traditum est, vel recuperatur in errantibus, vel in nuper credentibus instituitur.
3:7 .
3:8 <Ad murum.>Firmitatem scilicet et munimentum perfectae dilectionis in cordibus electorum, ad quam instructores ejus aedificando perveniunt, cum in operibus charitatis proficiendo dicunt: <Viam mandatorum tuorum cucurri,>etc. Psal. 118., illa, scilicet dilatatione mentis illustratae, quae et amicum in Deo et inimicum diligunt propter Deum.
3:9 .
3:10 .
3:11 .
3:12 .
3:13 <Et portam vallis.>BEDA, ubi supra. Vallis Josaphat, quae et Gehennon, etc., usque ad id est humiles dono supernae refectionis. <Et portam vallis.>ID., ibid. Bene post portam veterem et murum latioris plateae aedificatur porta vallis, quia post rudimenta catholicae fidei quae per dilectionem operatur, necesse est ut humilitas nobis quasi custos virtutum tenenda insinuetur, ut quanto magni sumus, humiliemur in omnibus. <Et mille.>ID., ibid. Ferunt quia situs urbis Jerusalem, etc., usque ad ut omnia a se rudera actionis noxiae inutilis locutionis et etiam superfluae cogitationis ejiciant.
3:14 .
3:15 <Et portam fontis aedificavit Sellum filius,>etc. ID., ibid. Narrant scriptores, etc., usque ad gradus ipsius Ecclesiae incrementis et profectibus bonorum operum, quae per humilitatem fiunt comparans. <De civitate David,>etc. Civitas David juxta litteram mons Sion appellatur, qui a meridie positus pro arce urbi supereminet, et major pars civitatis infra montem jacet in planitie humilioris collis sita. Unde in libro Regum: <Cepit autem David arcem Sion, haec est civitas David>II Reg. 5.. Et paulo post: <Habitavit autem David in arce, et vocavit eam civitatem David>Ibid..
3:16 <Aedificavit Nehemias filius Asboth,>etc. BEDA, ubi supra. Post portam fontis et muros piscinae Siloe, etc., usque ad et ad regnum coeleste ascensuros cognoscant. <Contra sepulcrum David,>etc. ID., ibid. Nota David non in Bethlehem, ut quidam putant, sed in Jerusalem esse sepultum certa ratione mysterii. Sicut enim in Bethlehem natus et in regem unctus Christum ibidem nasciturum de suo semine, et a magis sub persona regis adorandum figuravit; ita in Jerusalem defunctus et sepultus ipsum in eadem civitate passurum ac sepeliendum. <Ad piscinam, etc.>ID., ibid. Piscinam, Scripturam scilicet divinam, etc., usque ad et si hostis antiquus fontem nobis abstulerit verbi Dei, continuo in arcem mentis irrumpit.
3:17 .
3:18 .
3:19 <Et aedificavit,>etc. ID., ibid. Hucusque primus civitatis murus exstruitur, etc., usque ad in quo interni arbitrii oculos offendamus. <Contra ascensum,>etc. Hic est Christus qui in fide sua et dilectione Judaeorum populum adunavit et gentium: unde lapis angularis dicitur. Contra cujus ascensum mensura secunda aedificatur, cum per munditiam piae cogitationis ad visionem ejus tendimus, cum etiam in hac vita retenti crebro visionis ejus desiderio suspiramus. Sequuntur plurimi structorum ordines, qui mensuram secundam aedificasse narrantur: quia maxima Ecclesiae structura est in munimine interioris virtutis, cum scilicet omni custodia munimus cor nostrum, quoniam ex ipso vita procedit. Singula vero ad intelligentiam spiritualem trahere nimis longum est.
3:20 .
3:21 .
3:22 .
3:23 .
3:24 .
3:25 .
3:26 <Nathinaei.>Dicuntur Gabaonitae, qui in ministerium domus Domini juxta dispositionem Josue filii Nun fideli devotione serviebant. <In Ophel.>BEDA, ibid. Turris erat non longe a templo altitudine enormi. Unde Ophel, id est, tenebrarum vel nubili nomen accepit, quia nubibus caput inseruit. Denique ubi in Michaea scriptum est: <Et tu turris gregis nebulosa>Mich. 4., benedicitur turris Ophel. Haec turris in qua parte sit civitatis, liber Paralipomenon ostendit, dicens quod <Manasses aedificavit murum extra civitatem David ad occidentem Gihon in convalle, ab introitu portae piscium per circuitum usque ad Ophel>II Par. 33.. Conveniebat ergo juxta situm loci, ut ministri templi in vicina templo turre habitarent. Mystice autem Nathinaei habitant in Ophel, id est, in turre nebulosa, cum hi qui professione perfectioris vitae dicati sunt Deo, et in munimento et altitudine virtutum actione semper et cogitatione morantur, et conversatio eorum in coelis est, quos admirans vulgus ait: <Qui sunt isti qui ut nubes volant>Isa. 60., etc. Item Thecueni habitant in Ophel, cum quique religionis habitu insignes abdita Scripturarum de quibus scriptum est: <Tenebrosa aqua in nubibus aeris>Psal. 17., id est, mystica scientia in prophetis, illustrato corde penetrant, et assidue legunt, et meditantur. <Contra.>BEDA, ibid. Dominum scilicet, qui quotidie nos misericordiae suae gratia, ne in aerumnis vitae labamur praesentis et deficiamus, irrigat, etc., usque ad et quia post praesentia virtutum dona ad videndam claritatem Domini ascenditur, recte infertur: <Post eum aedificaverunt Thecueni,>etc.
3:27 <A turre.>A turre magna et eminenti usque ad murum templi pervenit structura civitatis, cum justi ab altitudine contemplationis, quae mentem in hac vita despectis temporalibus ad coelestia desideria suspendunt, veraciter in illa vita ad claritatem Dominicae incarnationis intuendam, patefacta etiam divinae aeternitatis gloria ascendunt. Et quia Thecua buccina vel tuba, Thecueni buccinatores interpretantur, apte dicitur quod Thecueni hoc aedificaverint. Doctorum enim est quorum sonus exit in omnem terram, praesentia Dei dona vel futura in civitate ejus, id est, fidelibus patefacere. <Ad murum templi.>Corpus scilicet Christi: de quo dicitur Joan. 2: <Solvite templum hoc, et in tribus diebus excitabo illud:>quod scilicet persecutores in morte solverant, sed excitatum et ad coelos exaltatum amatores videre non cessant in gloria.
3:28 <Sursum autem,>etc. BEDA, ibid. Hanc portam significat Jeremias esse in orientali plaga civitatis, etc., usque ad equi cum in bono accipiuntur, sicut asini, cameli, et muli, conversos ad Dominum populos Gentilium, vel curas rerum temporalium, Domino scilicet animae subjugatas ostendunt. <Aedificaverunt sacerdotes,>etc. Sacerdotes vero civitatis Dei murum ad portam aquarum aedificant, cum doctores post vocationem Judaeorum ad inducendos in Ecclesiam Gentiles, verbum Dei seminando perveniunt. BEDA, ibid. Longum est de singulis aedificiis vel aedificatoribus mystice disserere, etc., usque ad nomina conscribit in coelo.
3:29 .
3:30 .
3:31 .
4:1 . . . <Sanaballat . . . motus nimis subsannavit Judaeos.>BEDA in Esdram, lib. 3, cap. 19, tom. 2. Plane ira haereticorum est. Haec sunt verba eorum qui se Samaritanos cognominant, id est, custodes legis Dei, etc., usque ad et ornamenta virtutum moribus emendati consequantur.
4:2 .
4:3 <Sed et Tobias,>etc. ID., ibid. Hujus Tobiae persona et verba haereticis conveniunt, etc., usque ad quibus fructiferas fidelium mentes corrumpere nituntur.
4:4 .
4:5 .
4:6 .
4:7 .
4:8 .
4:9 <Et oravimus.>Hoc est unicum contra hostes Ecclesiae refugium, oratio scilicet ad Deum, et industria doctorum, qui die nocteque in lege ejus meditantes, corda fidelium contra insidias diaboli et militum ejus praedicando, consolando, et exhortando praemuniant.
4:10 <Humus nimia est.>BEDA, ibid. Congesta, scilicet, in loco muri, etc., usque ad et pro domo vel civitate procella tentationis ingruente, ruinam aedificasse probatur.
4:11 <Dixerunt hostes,>etc. Haec in aedificio spirituali agi solent; manet enim indefessus hostis cum satellitibus suis, spiritibus scilicet immundis et hominibus malignis, qui opera virtutum et fidei nobis incautis impedire et expugnare contendunt, et mentem fidelium mucrone pravae suggestionis interficere. Sed contra haec nobis armatura Dei sumenda est, ut possimus resistere in die malo, et in omnibus perfecti stare.
4:12 .
4:13 <Statui in loco,>etc. Ut agmine, scilicet armatorum circumdati operatores liberius et securius aedificarent. Dispertiti enim sunt gradus fidelium: alii bonis operibus intus ornantes Ecclesiam aedificant: alii armis sacrae lectionis muniti contra impugnantes haereticos vigilant. Hi religiosa devotione proximos in fidei veritate confortant, illi adversus diaboli vel vitiorum tela necessarium certamen exercent, et ab ovili dominico insidiantes lupos pastorali sollicitudine arcent.
4:14 .
4:15 .
4:16 <A die illa, media pars,>etc. BED., ibid., cap. 20. Notandum, quia non solum media pars juvenum faciebat opus, etc., usque ad minorisque laboris est incognitam cavere carnis voluptatem, quam rejicere cognitam.
4:17 .
4:18 .
4:19 .
4:20 .
4:21 .
4:22 <Et sint vobis vices per noctem et diem.>ID., ibid. Nota quantum studium in operando habuerint, etc., usque ad contradicentes arguendo ab Ecclesia repellunt.
4:23 .
5:1 <Et factus est clamor,>etc. BEDA in Esdr., lib. 3, cap. 21. Desiderabat populus murum construere, etc., usque ad et manus nostras a propriis voluptatibus avertens ad construendam Christi convertat civitatem. ID., ibid. Tribus de causis augetur clamor populi. Quidam fame coacti filios suos ditioribus vendere volebant: alii liberis parcentes agros potius et domos. Nonnulli, prohibentes liberorum et agrorum venditionem, hoc tantum persuadebant, ut mutuo sumerent pecuniam in tributa regis, oppignoratis agris et vineis, donec redeunte fertilitate reddere possent feneratoribus quod mutuo accepissent.
5:2 .
5:3 .
5:4 .
5:5 .
5:6 .
5:7 <Et increpavi optimates,>etc. BED., ibid. Tanquam dux optimus militiae coelestis et sapiens architectus civitatis Dei, etc., usque ad ut dimittat nobis Pater debita nostra.
5:8 .
5:9 .
5:10 .
5:11 .
5:12 .
5:13 <Insuper excussi sinum meum, et dixi: Sic excutiat Deus omnem,>etc. Huic sententiae terribilis clausula imponitur. Quicunque enim pauperibus misericordiam non impendit, vel ab eis qui non habent velut juste exigit, de domo sua excutitur, id est, de coetu Ecclesiae, in qua putabat se perpetuo manere, et de laboribus, scilicet, in fructibus bonorum operum, in quibus se laudabiliter laborasse putabat: nil prorsus laboris recipiet. Labores enim sine pietate non possunt apud Deum fructuosi esse. <Et dixit universa multitudo, Amen.>Quantum Nehemiae objurgatio vel imprecatio corda omnium moverit, ostenditur: audita enim contestatione respondentes, amen, et Deum collaudantes, fecerunt quae jusserat. In quo patet: quia non timore, sed amore dicta ejus susceperunt.
5:14 <Per annos duodecim.>BEDA, ubi supra. Hoc exponit Apostolus, dicens: <Quia statuit Dominus eos qui Evangelium annuntiant, de Evangelio vivere: ego autem nullo horum usus sum>I Cor. 9.. Duodecim autem annis Nehemias cum fratribus suis ita in ducatu vivebant, ut annonas quae ducibus debebantur, non comederent: insinuans evangelicum opus esse in regimine plebis, opus rectoris notabiliter circa Ecclesiam exercere, et a subditis commodum terrenum non quaerere.
5:15 .
5:16 .
5:17 .
5:18 .
5:19 .
6:1 .
6:2 <Veni, et percutiamus foedus,>etc. BEDA, in Esdr., lib. 3, cap. 23, tom. 2. Hostes sanctae civitatis persuadebant Nehemiae in campestria descendere, etc., usque ad quia versutus hostis semper manus nostras tentat impedire, curemus eas semper divino auxilio confortare.
6:3 .
6:4 .
6:5 .
6:6 .
6:7 .
6:8 .
6:9 .
6:10 <Et ingressus.>Pulsatus insidiis hostium Nehemias domum Samariae quasi amici et fratris ingreditur, sed ipsum insidiatorem et hostem invenit, tanquam externorum donis et amicitia corruptum. Semper enim electi habent foris pugnas, intus timores, nec solum apostoli, sed et prophetae periculis ex genere, periculis ex gentibus, periculis ex falsis fratribus suspectam vitam agebant.
6:11 .
6:12 .
6:13 .
6:14 .
6:15 <Mensis Eliud.>BEDA, ubi supra. Qui secundum Hebraeos sextus est, etc., usque ad et deinceps se in Dei et proximi dilectione in bonis operibus adornent.
6:16 <Universae gentes.>ID., ibid. Qui autem structores sanctae civitatis terrere volebant, etc., usque ad et ab Ecclesia expulsi.
6:17 .
6:18 .
6:19 .
7:1 <Postquam autem aedificatus est murus,>etc. BEDA, in Esdr., lib. 3, cap. 24, tom. 2.Mystice: ubi Ecclesiae murus collectis ad fidem novis populis vel correctis his qui erraverant aedificatus fuerit, mox ponendae sunt valvae regularis disciplinae; ne diabolus, qui quasi leo rugiens circumit, in ovile Dei possit irrumpere. <Recensui janitores.>ID., ibid. Janitores qui, scilicet, claves regni coelorum perceperunt, ut dignos et humiles suscipiant; superbos vero et indignos ab ingressu supernae civitatis arceant, dicendo: <Non est tibi pars, neque sors in sermone hoc. Cor enim tuum non est rectum coram Deo>Act. 8..
7:2 .
7:3 <Non aperiantur portae . . . usque ad calorem solis,>etc. ID., ibid., id est, toto tempore noctis, etc., usque ad unde: <Et portae ejus non claudentur per diem, nox non erit illic>Apoc. 21.. <Et posui custodes.>ID., ibid. Custodes animarum non sunt de neophytis vel de plebe constituendi, sed de illis qui a certamine vitiorum Dei gratia liberati, jam mentem in Jerusalem, id est in visione tranquillae pacis habent, quorum conversatio in coelis est. Aliis enim consummato cursu de hac luce subtractis, alii mox substituuntur, nec unquam desinit qui pacem Ecclesiae excubando ambiant propter timores nocturnos, secundum illud: <Pro patribus tuis nati sunt tibi filii>Psal. 44.. Haec illi tempori typice conveniunt, quo seminato per apostolos longe lateque Dei verbo, totus orbis novum fidei germen accepit, necdum Ecclesiae aedificatae, sed tantum auditu verbi et sacramentis populi adhuc rudes erant imbuti. Ideo congregatis non solum optimatibus, sed et vulgo diligenter eorum numerum recensere curavit, ut perspecta omnium summa, discernere posset, qui in Jerusalem, qui in aliis civitatibus habitare deberent. <Unumquemque contra domum.>Sic enim custodia sanctae Ecclesiae rite perficitur, si quisque ita sollicitudinem omnium fidelium gerat, ut specialiter eis quibus Deo auctore praelatus est, curam diligentioris studii impendat.
7:4 .
7:5 .
7:6 .
7:7 .
7:8 .
7:9 .
7:10 .
7:11 .
7:12 .
7:13 .
7:14 .
7:15 .
7:16 .
7:17 .
7:18 .
7:19 .
7:20 .
7:21 .
7:22 .
7:23 .
7:24 .
7:25 .
7:26 .
7:27 .
7:28 .
7:29 .
7:30 .
7:31 .
7:32 .
7:33 .
7:34 .
7:35 .
7:36 .
7:37 .
7:38 .
7:39 .
7:40 .
7:41 .
7:42 .
7:43 .
7:44 .
7:45 .
7:46 .
7:47 .
7:48 .
7:49 .
7:50 .
7:51 .
7:52 .
7:53 .
7:54 .
7:55 .
7:56 .
7:57 .
7:58 .
7:59 .
7:60 .
7:61 .
7:62 .
7:63 .
7:64 .
7:65 .
7:66 <Omnis multitudo quasi vir unus.>JOSE. Populum etiam agros colentem decimas fructuum ad Hierosolymam jussit offerre, ut habentes sacerdotes et levitae alimenta, perpetua religionis jura non derelinquerent. Et hi quidem libenter sequebantur decreta Nehemiae. Civitatem vero exinde contigit hominum multitudine compleri. Multa etiam talia bona et laudibus digna cum summa fecisset magnificentia Nehemias, ad senectutem perveniens defunctus est.
7:67 .
7:68 .
7:69 .
7:70 .
7:71 .
7:72 .
7:73 .
8:1 <Et venerat mensis septimus.>BEDA, in Esdr., lib. 3, cap. 26, tom. 2. Qui non longe aberat. Cum enim murus esset vicesima quinta die sexti mensis completus quinque tantum dies ad exordium septimi mensis supererant, qui a prima die usque ad vicesimam secundam totus legitimis caeremoniis consecratus est, quibus ita rite celebratis, deinde ad disponendos urbis mansores cum principibus et plebe reversus est. <Congregatusque omnis populus quasi vir unus ad plateam quae,>etc. BED., ibid. Notanda devotio populi et concordia, etc., usque ad quarum cantu populus inter orationes et hostias ardentius ad legis memoriam excitaretur. Mystice quoque aedificata civitate oportet sequi lectionem divinam, et tubas sonare crebriores, ut populus, sacramentis coelestibus initiatus etiam sacris eloquiis ex tempore solertius qualiter vivere debeat, instruatur. <Ante portam aquarum.>ID., ibid. Portam aquarum reor dici in atrio sacerdotum, etc., usque ad bene ergo ante portam aquarum collectus est populus tanquam per antistitem suum fluentis Scripturarum spiritualiter potandus.
8:2 .
8:3 .
8:4 <Stetit autem Esdras scriba super gradum.>ID., ibid. Videtur meminisse, etc., usque ad ut timidus et erubescens quae ipse non fecit, aliis faciendo praedicit, etc. Cum praesules quantum honore, tantum opere subditos antecellunt, ipsi eorum exemplis incitati vitae suae gradum exsequuntur devoti, et ab eis pie admoniti pro peccatis vel desiderio patriae coelestis lacrymas profundere delectantur. Unde bene subditur, flebat enim omnis populus, cum audiret verba legis, etc.
8:5 .
8:6 .
8:7 .
8:8 .
8:9 .
8:10 <Ite et comedite,>etc. ID., ibid. Ut infirmiores conscientias proximorum exemplo piae actionis, et suavitate devotae admonitionis confortemus, praecipit, quatenus sicut adipe et pinguedine repleantur, aliisque exsultationis laudent nomen Domini. Juxta litteram quoque cum in festis diebus post orationem lectionem et psalmorum studia completa carnem reficimus, pauperum et peregrinorum meminisse debemus. <Nolite contristari.>Doctores qui mentes auditorum sacris lectionibus ad lacrymas excitant, et iidem consolantur, dum gaudia secutura promittunt.
8:11 <Quia dies sanctus est.>Dum scilicet verbis Domini audiendis et implendis operam damus, in quo die quamvis exterius adversa patientes, nos in spe gaudere oportet. Unde: <Quasi tristes, semper autem gaudentes>II Cor. 6., etc.
8:12 .
8:13 .
8:14 <Et invenerunt,>etc. BEDA, ubi supra. Haec in Levitico plenius scripta sunt, etc., usque ad meminisse nos oportet, quia <incolae sumus in terra, et peregrim sicut patres nostri>Psal. 38..
8:15 <Ligni pulcherrimi,>etc. ID., ibid. Quod scilicet Hebraei vocant cedrum. Frondes vero myrti et obumbraculum sibi affert qui dicere potest, <Christi bonus odor sumus Deo in omni loco>II Cor. 2.. <Frondes ligni pulcherrimi et frondes myrti,>etc. Fructus charitatis quae inter omnes virtutes pulcherrima, per quam et Christus lignum crucis pro nostra salute ascendit, cujus passionem dum quantam possumus, imitamur, frondibus profecto ligni pulcherrimi et myrti in mortificationem vitiorum et libidinum protegimur. Magi enim Domino myrrham offerentes docuerunt, quia qui Christ sunt, carnem suam cum vitiis et concupiscentiis crucifigunt. <Ramos palmarum.>ID., ibid. Ramos palmarum afferimus, quod est manus victricis ornatus, cum mentem vitiorum victricem gerimus, et invictam cunctis hostibus, ut stemus <ante thronum in conspectu Agni, amicti stolis albis, et palmae in manibus nostris>Apoc. 7..
8:16 <Unusquisque in domate.>ID., ibid. In domate suo, etc., usque ad et in majoribus bonae actionis frugibus abundare mereamur.
8:17 .
8:18 <Septem diebus.>ID., ibid. Scenopegia septem diebus agebatur, etc., usque ad in conspectu Dei congregatus, et nunquam segregandus  exsultaverit.
9:1 <In die autem,>etc. BEDA, ubi supra, cap. 28. Notanda correcti populi devotio, etc., usque ad cum a caeteris laboribus cessatur divinis lectionibus aures liberius commodantur. <Et dixit Esdras.>Usque ad finem orationis vel confessionis ejus, quod supra dictum est, quia confitebantur peccata sua, et peccata patrum suorum, plenius Esdra deprecante qualiter factum sit, ostenditur.
9:2 .
9:3 .
9:4 .
9:5 .
9:6 .
9:7 .
9:8 .
9:9 .
9:10 .
9:11 .
9:12 .
9:13 .
9:14 .
9:15 .
9:16 .
9:17 .
9:18 .
9:19 .
9:20 .
9:21 .
9:22 .
9:23 .
9:24 .
9:25 .
9:26 .
9:27 .
9:28 .
9:29 .
9:30 .
9:31 .
9:32 .
9:33 .
9:34 .
9:35 .
9:36 .
9:37 .
9:38 <Super omnibus ergo his.>BEDA, in Esdr., lib. 3, tom. 2. Ostendit quanta devotione omnes personae eorum novum post festa scenopegiae conventum fecerint, ut scilicet tota se intentione a scelerum contagiis expurgatos divino foederi conjungerent, et foederis sancti conditionem sermone firmarent et scripto; et sic ab impiorum consortio separati, securiores replerent opus, quod dudum coeperant, id est, ut congruos urbis cives de numero impiorum instituerent.
10:1 <Signatores,>etc. Alia translatio: <Nehemias qui Athersata fertur,>erat enim dionymos unde singulariter subjungitur filius Achelai, quod superius apertius dicitur: <Dixit autem Nehemias, et ipse est Athersata>II Esd. 7.. ID., ibid. Nobis quoque sabbatum spirituale semper agendum est, etc., usque ad primo die a malis emundare necesse est, deinde bonis actibus adornari.
10:2 .
10:3 .
10:4 .
10:5 .
10:6 .
10:7 .
10:8 .
10:9 .
10:10 .
10:11 .
10:12 .
10:13 .
10:14 .
10:15 .
10:16 .
10:17 .
10:18 .
10:19 .
10:20 .
10:21 .
10:22 .
10:23 .
10:24 .
10:25 .
10:26 .
10:27 .
10:28 .
10:29 .
10:30 .
10:31 .
10:32 .
10:33 .
10:34 .
10:35 .
10:36 .
10:37 .
10:38 .
10:39 .
11:1 <Habitaverunt autem.>BEDA, in Esdr., lib. 3, tom. 2. Nunc completa est dispositio, etc., usque ad sed occulti judicis et largitoris munere percipiunt.
11:2 .
11:3 <Et in Jerusalem.>ID., ibid. His verbis ostenditur, etc., usque ad nam sequitur: <De filiis Juda habitaverunt in Cariatharbe>II Esd. 11.. ID., ibid. Judas interpretatur <confitens>vel <confessio,>etc., usque ad cum eorum qui sunt in agro unus assumetur, et alter relinquetur.
11:4 .
11:5 .
11:6 .
11:7 .
11:8 .
11:9 .
11:10 .
11:11 .
11:12 .
11:13 .
11:14 .
11:15 .
11:16 .
11:17 .
11:18 .
11:19 .
11:20 .
11:21 .
11:22 .
11:23 .
11:24 .
11:25 .
11:26 .
11:27 .
11:28 .
11:29 .
11:30 .
11:31 .
11:32 .
11:33 .
11:34 .
11:35 .
11:36 .
12:1 <Hi sunt autem,>etc. BEDA, ubi supra. Hic principes sacerdotum una cum fratribus suis, etc., usque ad ideoque illum superstite adhuc Nehemia nasci potuisse. BED., ibid. Descripta est successio principum sacerdotum et levitarum, etc., usque ad amica provisione restaurant, et debito honore exaltant. ID., ibid. Jamdudum aedificata erat civitas, etc., usque ad tunc in re ipsa fruentium divina visione beatorum hominum in corporibus spiritualibus inter angelica agmina.
12:2 .
12:3 .
12:4 .
12:5 .
12:6 .
12:7 .
12:8 .
12:9 .
12:10 .
12:11 .
12:12 .
12:13 .
12:14 .
12:15 .
12:16 .
12:17 .
12:18 .
12:19 .
12:20 .
12:21 .
12:22 .
12:23 .
12:24 .
12:25 .
12:26 .
12:27 <Et in cymbalis,>etc. ID., ibid. Cymbala, psalteria et citharae, etc., usque ad et corda proximorum ad amorem ejus accendunt, etc.
12:28 <Filii cantorum.>ID., ibid. Imitatores scilicet eorum, etc., usque ad et in ipso tempore dedicationis, id est, perpetuae remunerationis ibidem pariter uniuntur.
12:29 .
12:30 <Et mundati.>ID., ibid. Justus omnino ordo, etc., usque ad et vinctis manibus et pedibus in tenebras exteriores mittatur.
12:31 <Principes Juda,>etc., id est confessionis vel laudis, perfectiores sunt Ecclesiae doctores, qui in dedicatione civitatis super murum ascendunt: quia in tempore tribulationis generalem Ecclesiae conversationem altius vivendo transcendisse probantur. De his enim dicitur: <Super muros tuos Jerusalem constitui custodes>Isai. 62., etc. Jure ergo qui nunc muris Ecclesiae tanquam vigiles praesunt, tunc quoque eisdem gloria remunerationis praeeminebunt. <Et statui duos choros.>Vel spurcitiam peccatorum in praesenti de Ecclesia correctius vivendo, et errantes corrigendo expurgent, et in futuro qui corrigi noluerunt, judiciaria potestate de civitate Domini, id est ingressu patriae coelestis, expellant. Vel chori laudantium ad dexteram euntes super murum ad portam sterquilinii ascendunt, dum eos dignos laude praedicant qui omnem immunditiam de Ecclesia praedicando, arguendo, excommunicando, eliminant.
12:32 .
12:33 .
12:34 <Et de filiis sacerdotum,>etc. In hac  vita filii sacerdotum in dedicatione civitatis Dei tubis canunt, qui ad memoriam patriae coelestis corda auditorum praedicando succendunt.
12:35 <In vasis cantici David.>Quia non suo sensu vel desideriis innituntur, sed patrum et prophetarum vitam et doctrinam per omnia sequentes verbo praedicationis insistunt. <Et Esdras scriba.>Quia in omnibus quae agunt verba sanctae Scripturae prae oculis habent, quibus ducibus ad ingressum vitae aeternae perveniant, ut inebrientur ab ubertate domus Dei, et torrente voluptatis ejus, apud quem est fons vitae, etc.
12:36 <Et contra eos ascenderunt in gradibus.>BED., ibid. Cum civitas aedificaretur, etc., usque ad sanctam Trinitatem socia exsultatione concelebrant. <Super domum David.>ID., ibid. Ascendunt filii sacerdotum, etc., usque ad ortumque solis justitiae sine occasu videre merentur.
12:37 <Et super turrim furnorum.>ID., ibid. Longum est de singulis portis vel turribus specialiter disserere, etc., usque ad circumspecta semper est necesse cautela et sollicitudine muniri.
12:38 .
12:39 <Steteruntque duo chori.>ID., ibid. Perambulatis moenibus et portis civitatis, etc., usque ad in cujus implenda voluntate infatigabili mente permaneo.
12:40 .
12:41 <Et clare cecinerunt. Dirupisti vincula mea: tibi sacrificabo hostiam laudis. Vota mea Domino reddam in atriis domus Domini, in conspectu omnis populi in medio tui, Jerusalem>Psal. 115.. Hoc fit cum in coelesti patria omnium sanctorum multitudine congregata, eas pro quibus in praesenti gemimus quasque gratiarum actione quotidiano desiderio sitimus, laudes efferimus.
12:42 <Deus enim laetificaverat,>etc. BEDA, ibid. Haec ad dedicationem sanctae civitatis, etc., usque ad et victimas bonorum operum maximas immolare curabant. <Sed et uxores,>etc. Quia tempore resurrectionis non solum qui praedicando vel fortiter operando Ecclesiam aedificarunt, fructum laboris percipiunt, sed etiam infirmiores ejusdem fidei consortes, eadem vitae perceptione laetantur. <Benedixit>enim <Dominus pusillis cum majoribus>Psal. 113..
12:43 <Recensuerunt quoque,>etc. BEDA, ibid. Gaudentibus cunctis, etc., usque ad et cum magna laude et laetitia erat dedicata.
12:44 .
12:45 .
12:46 .
13:1 <In die autem illo lectum,>etc. BEDA in Esdr., lib. 3, tom. 2. Hi quia de incestu nati sunt, etc., usque ad sed Deus maledictionem Balaam in benedictionem populi sui convertit, et eum ab armis inimicorum munivit.
13:2 .
13:3 <Factum est autem,>etc. Quia necesse est nos auditui veritatis intendere, ubi cum ab uno quolibet vitio lectione divina prohibemur, continuo etiam quidquid vitii sordidantis in nobis deprehendimus, ab actione nostra et conscientia repellamus.
13:4 .
13:5 .
13:6 <In omnibus his.>BED., ibid. Haec sententia, etc., usque ad quae reversus Nehemias mox exstirpavit.
13:7 .
13:8 <Et projeci vasa domus Tobiae.>ID., ibid. Qui supradictus est servus Ammonites inimicus populi Dei, etc., usque ad quae communicatio haereticis et schismaticis cum orthodoxis et pacificis filiis Dei. <Et projeci.>ID., ibid. Tu quoque quidquid inter fideles infidelitatis et immunditiae repereris, etc., usque ad sicut enim Nehemias in caeteris, ita et in hoc ejus personam repraesentavit. ID., ibid. Sex diebus operari per legem quae necessaria sunt, in septima jubemur quiescere, etc., usque ad ab his quae ad victum et vestitum pertinent, prorsus intuitum avertant, sed congruo moderamine dispensent.
13:9 .
13:10 .
13:11 .
13:12 .
13:13 .
13:14 .
13:15 .
13:16 <Inferentes pisces et omnia,>etc. ID., ibid. Piscis bonus, est pia fides, quam qui a Domino malus, infirma cogitatio quae se curis hujus mundi ultra modum immergit, quam nobis Tyrii, id est, coangustati quaerunt in sabbato vendere, cum immundi spiritus quietem nostrae cogitationis importune profundis saeculi curis obruere tentant. Sed pro tali mercatu Nehemias optimates objurgat Juda et castigat, dum divina inspiratio eos qui professioni pietatis servire conantur, ab hujusmodi cogitationibus purgat.
13:17 .
13:18 .
13:19 <Factum est autem.>BED., ibid. Conscientia nostra, etc., usque ad ideo merito sequitur.
13:20 .
13:21 .
13:22 <Dixi quoque Levitis ut mundarentur,>etc. ID., ibid. Mundari eos a quotidiano operis exercitio necesse est, etc., usque ad oportet ut prius mentem suam et actus purificent ab omni errorum labe.
13:23 <Sed et in diebus illis.>ID., ibid. In Ecclesia uxores alienigenas ducunt, etc. usque ad sed gentili sensu interpretantes.
13:24 .
13:25 .
13:26 .
13:27 .
13:28 .
13:29 .
13:30 <Igitur mundavi eos.>ID., ibid. Apertius per omnia finis et condignus aedificio sanctae civitatis et templi Domini, ut emundatis civibus alienae a Deo pollutionis, ordines sacerdotum et levitarum in ministerio suo rite custodiantur, ut instituti regulariter magistri Ecclesiae castigatum ab omni peccato populum de caetero in bono permanere, et crescere semper exhortentur.
13:31 <Et in oblatione lignorum in temporibus constitutis et in primitiis.>Inter caetera populus ligna offert ad ignem altaris nutriendum, cum opera virtutum divina consecratione digna operantur. Ardent ligna et consumuntur in altari holocaustorum, cum in cordibus electorum, opera justitiae flamma charitatis perficiuntur. <Memento mei, Deus, in bonum.>Merito talis conditor et dedicator civitatis, post multos devotionis labores, memoriae se Creatoris et largitoris omnium bonorum commendat.
