Attribute VB_Name = "modMain"
' NB: if you get a linker error, move the project from a VMWare shared folder to a folder on the VM's main drive

Option Explicit
Private x(1 To 73) As String

Sub Main()
    Dim i As Long, s As String
    x(1) = "Gn"
    x(2) = "Ex"
    x(3) = "Lv"
    x(4) = "Nm"
    x(5) = "Dt"
    x(6) = "Jos"
    x(7) = "Jdc"
    x(8) = "Rt"
    x(9) = "1Rg"
    x(10) = "2Rg"
    x(11) = "3Rg"
    x(12) = "4Rg"
    x(13) = "1Par"
    x(14) = "2Par"
    x(15) = "Esr"
    x(16) = "Neh"
    x(17) = "Tob"
    x(18) = "Jdt"
    x(19) = "Est"
    x(20) = "Job"
    x(21) = "Ps"
    x(22) = "Pr"
    x(23) = "Ecl"
    x(24) = "Ct"
    x(25) = "Sap"
    x(26) = "Sir"
    x(27) = "Is"
    x(28) = "Jr"
    x(29) = "Lam"
    x(30) = "Bar"
    x(31) = "Ez"
    x(32) = "Dn"
    x(33) = "Os"
    x(34) = "Joel"
    x(35) = "Am"
    x(36) = "Abd"
    x(37) = "Jon"
    x(38) = "Mch"
    x(39) = "Nah"
    x(40) = "Hab"
    x(41) = "Soph"
    x(42) = "Agg"
    x(43) = "Zach"
    x(44) = "Mal"
    x(45) = "1Mcc"
    x(46) = "2Mcc"
    x(47) = "Mt"
    x(48) = "Mc"
    x(49) = "Lc"
    x(50) = "Jo"
    x(51) = "Act"
    x(52) = "Rom"
    x(53) = "1Cor"
    x(54) = "2Cor"
    x(55) = "Gal"
    x(56) = "Eph"
    x(57) = "Phlp"
    x(58) = "Col"
    x(59) = "1Thes"
    x(60) = "2Thes"
    x(61) = "1Tim"
    x(62) = "2Tim"
    x(63) = "Tit"
    x(64) = "Phlm"
    x(65) = "Hbr"
    x(66) = "Jac"
    x(67) = "1Ptr"
    x(68) = "2Ptr"
    x(69) = "1Jo"
    x(70) = "2Jo"
    x(71) = "3Jo"
    x(72) = "Jud"
    x(73) = "Apc"
    s = "#"
    s = s & GetDiffs("lat", "d:\projects\vulsearch4\text\")
    s = s & GetDiffs("eng")
    s = s & GetDiffs("fra")
    s = s & GetDiffs("stu")
    s = s & GetDiffs("glo")
    s = s & GetDiffs("cra")
    s = s & GetDiffs("gla")
    i = FreeFile
    Open "d:\projects\vulsearch4\website\htdocs\update.txt" For Output As #i
    Print #i, "# Update file for VulSearch versions 4.2, 4.1 and 3.2"
    Print #i, "# generated on the " & Format$(Now(), "d/m/yy")
    Print #i, "# ==================================================="
    Print #i, "#"
    Print #i, "# To update the texts in VulSearch with the corrections in this file,"
    Print #i, "# you must save this file to your hard disk, then while running VulSearch"
    Print #i, "# select the ""Update texts..."" option from the File menu and browse to the"
    Print #i, "# saved copy of this file."
    Print #i, s
    Print #i, "@"
    Close #i
    Beep
End Sub

Function VN(ByVal s As String) As Long
    VN = Val(Left$(s, InStr(s, ":") - 1))
    s = Mid$(s, InStr(s, ":") + 1)
    VN = VN * 1000 + Val(Left$(s, InStr(s, " ") - 1))
End Function

Function GetDiffs(ext As String, Optional UpdatedPath As String = "d:\projects\vulsearch4\textother\", Optional updext As String = "")
    Dim bk As Long, i As Long, j As Long, p As String, q As String, prfx As String
    Dim diff As Long
    Debug.Print ext
    If updext = "" Then updext = ext
    For bk = 1 To 73
        DoEvents
        prfx = vbCrLf & "@" & x(bk)
        i = FreeFile
        Open "d:\projects\vulsearch4\scripts\maketextupdate\canonical\" & x(bk) & "." & ext For Input As #i
        j = FreeFile
        Open UpdatedPath & x(bk) & "." & updext For Input As #j
        While Not EOF(i)
            Line Input #i, p
            Line Input #j, q
            diff = VN(p) - VN(q)
            If diff > 0 Then
                Do
                    GetDiffs = GetDiffs & prfx & vbCrLf & q
                    prfx = ""
                    Line Input #j, q
                Loop Until VN(p) = VN(q)
            End If
            If diff < 0 Then
                Do
                    GetDiffs = GetDiffs & prfx & vbCrLf & Left$(p, InStr(p, " ")) & "X"
                    prfx = ""
                    Line Input #i, p
                Loop Until VN(p) = VN(q)
            End If
            
            If p <> q Then
                GetDiffs = GetDiffs & prfx & vbCrLf & q
                prfx = ""
            End If
        Wend
        Close #j
        Close #i
    Next bk
    If Len(GetDiffs) Then GetDiffs = vbCrLf & "*" & ext & GetDiffs
End Function
