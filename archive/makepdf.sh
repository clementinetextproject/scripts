#!/bin/bash

LATEXDIR=${1:-.}
(
cd $LATEXDIR
echo Creating vulgate.pdf...
sed -i -e "/versal{32}/s/19/13/" Dt.tex
pdflatex vulgate.tex >/dev/null
pdflatex vulgate.tex >/dev/null
pdftk titlea4.pdf vulgate.pdf cat output tmp.pdf
mv tmp.pdf vulgate.pdf
echo Creating twocolumn.pdf...
cp 1Jo.tex 1Jo.tmp
sed -e '$s/^\(.*\)$/\1\\enlargethispage{\\baselineskip}/' 1Jo.tmp > 1Jo.tex
pdflatex twocolumn.tex >/dev/null
pdflatex twocolumn.tex >/dev/null
mv 1Jo.tmp 1Jo.tex
pdftk titleUS.pdf twocolumn.pdf cat output tmp.pdf
mv tmp.pdf twocolumn.pdf
echo Updating pdf bookmarks and metadata...
perl ../pdfinfo.pl vulgate 4 5 7 8 10
perl ../pdfinfo.pl twocolumn 4 5 7 8 10
cp vulgate.pdf ../../../Website/htdocs/vulgata-1col.pdf
cp twocolumn.pdf ../../../Website/htdocs/vulgata-2col.pdf
)

