#!/usr/bin/perl
# 4/1/04 - little.mouth@soon.com - GPL
# Perl script to 
# Requirements: perl, sed
# 
# Usage: perl .pl
# 
# Output: 
# 

($file,$hujus,$decreta,$const,$offic,$start)=@ARGV;

print OUT "InfoValue: Biblia Sacra juxta Vulgatam Clementinam\n";
print OUT "InfoValue: http://vulsearch.sf.net/gettext.html\n";

open(OUT,"> outlines.txt") || die "can't open outlines.txt";

print OUT "1 0 1 $hujus Pr\x{e6}fationes\r\n";
print OUT "2 1 1 $hujus Pr\x{e6}fatio hujus editionis\r\n";
print OUT "3 1 2 $decreta Decreta Concilii Tridentini\r\n";
print OUT "4 1 3 $const Constitutio Clementis VIII\r\n";
print OUT "5 1 4 $offic Pr\x{e6}fatio ad lectorem officialis\r\n";

open(DATA, 'sed -n -e "9,83s/^.*{subsection}{\(.*\)}{\(.*\)}/\1#\2/p"' . " $file.toc |")    || die "can't fork sed";
$i=5;
while ($x=<DATA>)
{
  ($book,$page)=split /#/, $x;
  $page+=$start;
  $book =~ s/\\ae /\xe6/g;
  $book =~ s/\\ae\\ /\xe6 /g;
  $book =~ s/\\"e/\xeb/g;
  $i++;
  $j=$i-4;
  print OUT "$i 0 $j $page $book\r\n";
}
close(DATA);
close(OUT);

`mbtPdfAsm -m$file.pdf -dout.pdf -ooutlines.txt` &&
`mbtPdfAsm -mout.pdf -u -sAMichael_Tweedale_(ed.) -sTBiblia_Sacra_juxta_Vulgatam_Clementinam -sKhttp://vulsearch.sf.net/gettext.html` &&
 `pdftk out.pdf update_info pdfdata.txt output $file.pdf dont_ask`;
`rm out.*`;
