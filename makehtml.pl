#!/usr/bin/perl
# 12/1/04 - clementinevulgateproject@mail.com - GPL
# updated 16/11/13 - clementinevulgateproject@mail.com  - GPL
# updated 1/10/17 - clementinevulgateproject@mail.com  - GPL
# Perl script to generate valid XHTML 1.0 from VulSearch Latin source files
# Requirements: perl, sed
# 
# Usage: perl makehtml.pl BASE_DIRECTORY
# BASE_DIRECTORY: git clone git@bitbucket.org:clementinetextproject/vulsearch4.git and run  ./update-git-submodules.sh
# 
# Output: 
# files html/ABBREV.html for each abbreviation ABBREV of a book of the Bible
# 
# Background and documentation at http://vulsearch.sf.net/plain.html
# The main bulk of the conversion is done using makehtml.sed: this script
# just adds a header and footer, and deals with a couple of eccentricities
# (see inline comments passim).

$ENV{'LANG'} = 'C';

$basedir = $ARGV[0];
$source=$basedir . '/Text/'; #source directory
$ext='lat'; #file extension
$html=$basedir . '/Website/htdocs/html'; #html output directory
$indexfile=$html . '/index.html';
$data = $basedir . '/Scripts/data.txt';
$sedscript = $basedir . '/Scripts/makehtml.sed';

#initialize our hash of book names
@bib =
(
  'Gn', 'Ex', 'Lv', 'Nm', 'Dt', 'Jos', 'Jdc', 'Rt', '1Rg', '2Rg', '3Rg', '4Rg', '1Par', '2Par',
  'Esr', 'Neh', 'Tob', 'Jdt', 'Est', 'Job', 'Ps', 'Pr', 'Ecl', 'Ct', 'Sap', 'Sir', 'Is', 'Jr',
  'Lam', 'Bar', 'Ez', 'Dn', 'Os', 'Joel', 'Am', 'Abd', 'Jon', 'Mch', 'Nah', 'Hab', 'Soph', 'Agg',
  'Zach', 'Mal', '1Mcc', '2Mcc', 'Mt', 'Mc', 'Lc', 'Jo', 'Act', 'Rom', '1Cor', '2Cor', 'Gal',
  'Eph', 'Phlp', 'Col', '1Thes', '2Thes', '1Tim', '2Tim', 'Tit', 'Phlm', 'Hbr', 'Jac', '1Ptr',
  '2Ptr', '1Jo', '2Jo', '3Jo', 'Jud', 'Apc'
);

#for special characters in the book names
sub cleanup
{
  my ($s)=@_;
  $s =~ s/\xe6/\&aelig;/g;
  $s =~ s/\xeb/\&euml;/g;
  $s =~ s/\x9c/\&oelig;/g;
  return $s;
}

($dd,$mm,$yy)=(localtime(time))[3,4,5];
$yy=sprintf("%02d",$yy % 100);
$mm++;

foreach my $book (@bib)
{
  open(DATA, "sed -n -e \"/^$book\\//p\" $data |")    || die "can't fork sed";
  ($_,$long,$short,$create,$proof)=split /\//, <DATA>;
  close DATA;
  $long=cleanup($long);
  $short=cleanup($short);
  chomp($proof);
  $w = 's/<span.class=.active.>\(.*\)<.*/<a href="index.html">\1<\/a>/';
  `sed -n -e '1,/content[^=-]/p' $indexfile | sed -e '$w' > $html/$book.html`;
  open(OUT,">> $html/$book.html") || die "can't open $html/$book.html";

  print OUT '<div id="text">', "\n";
  print OUT "<h3>$long</h3>", "\n";

  #print main body of the book, mostly converted by a sed script
  open(IN, "sed -f $sedscript $source/$book.$ext | tr -d '\r' |")    || die "can't fork sed";
  $x=<IN>;
  chomp($x);
  if ($book eq 'Abd' || $book eq 'Phlm' || $book eq '2Jo' || $book eq '3Jo' || $book eq 'Jud')
  {
    $x=~s/<span class="chapter-num">1<\/span> //;
  } #Abd, Phlm, {2,3}Jo, Jud have only one chapter
  print OUT "$x\n";

  while ($x=<IN>)
  {
    print OUT "$x";
  }
  close(IN);

  #print foot
  if(0) {
  print OUT '<p lang="en" style="margin-top: -1em; text-align: center;">', "\n";
  print OUT '<a href="http://validator.w3.org/check?uri=referer"><img
  style="border: 0;"', "\n";
  print OUT '      src="valid-xhtml10.png" alt="Valid XHTML 1.0!" height="31"', "\n";
  print OUT '      width="88" /></a>', "\n";
  print OUT '  <a href="http://jigsaw.w3.org/css-validator/check/referer"><img
  style="border: 0;"', "\n";
  print OUT '      src="vcss.png" alt="Valid CSS!" height="31" width="88" /></a>', "\n";
  print OUT '</p>', "\n";
}
  print OUT '</div>', "\n";
  print OUT '</div>', "\n";
  print OUT '</div>', "\n";
  print OUT '</body>', "\n";
  print OUT '</html>', "\n";

  close(OUT);
  $c="s/^ *<li><a href=\\(.\\)$book.html\\(.\\)>\\(.*\\)<\\/a><\\/li>/<li><span class=\\1active\\2>\\3<\\/span><\\/li>/";
  `sed -i -e "$c" $html/$book.html`;
}

