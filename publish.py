#!/usr/bin/env python3

import os
from subprocess import Popen, PIPE
import time
import readline
from glob import glob
import codecs
from collections import OrderedDict as OD
import zipfile
import tempfile

BASEDIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
PUBDATE = time.strftime('%a, %d %b %Y %H:%M:%S %Z')


def edit_rss():
    RSSFILE = os.path.join(BASEDIR, 'Website/htdocs/clemtext.rss')
    IDXFILE = os.path.join(BASEDIR, 'Website/htdocs/index.html')
    rss = open(RSSFILE).readlines()
    last_build = -1
    for i, l in enumerate(rss):
        if l.find('lastBuildDate') >= 0:
            if last_build != -1:
                print(l)
                raise Exception(
                    f'Duplicate lastBuildDate lines {last_build} and {i}')
            last_build = i
    if last_build == -1:
        raise Exception('Missing lastBuildDate line')
    linkdate = time.strftime('%y%m%d')
    titledate = time.strftime('%d/%m/%y')
    idxdate = time.strftime('%b %d %Y')
    rss = rss[:last_build] + [
        f'    <lastBuildDate>{PUBDATE}</lastBuildDate>\n',
        f'    <item>\n',
        f'      <title>{titledate} - corrections to the text</title>\n',
        f'      <link>http://vulsearch.sf.net/{linkdate}</link>\n',
        f'      <description>Corrections to\n',
        f'        [ ]\n',
        f'      </description>\n',
        f'      <pubDate>{PUBDATE}</pubDate>\n',
        f'    </item>\n'
    ] + rss[last_build + 1:]
    with open(RSSFILE, 'w') as fh:
        fh.write(''.join(rss))
    pr = Popen(['gvim', f'+{last_build + 6}', RSSFILE])

    idx = open(IDXFILE).readlines()
    last_build = -1
    for i, l in enumerate(idx):
        if l.find('ongoing') >= 0:
            if last_build != -1:
                print(l)
                raise Exception(
                    f'Duplicate ongoing lines {last_build} and {i}')
            last_build = i + 1
    if last_build == -1:
        raise Exception('Missing ongoing line')
    linkdate = time.strftime('%y%m%d')
    titledate = time.strftime('%d/%m/%y')
    idx = idx[:last_build] + [
        f'    <b>{idxdate}</b>.</p>\n'] + idx[last_build + 1:]
    with open(IDXFILE, 'w') as fh:
        fh.write(''.join(idx))
    latestfile = os.path.join(BASEDIR, 'Website/htdocs/latest.txt')
    latest = int(open(latestfile).read()) + 1
    with open(latestfile, 'w') as fh:
        print(latest, file=fh)
    print(f'Updated website latest.txt to {latest}')

def patch_epub(epub):
    OPF = 'content.opf'
    NCX = 'toc.ncx'

    # generate a temp file
    tmpfd, tmpname = tempfile.mkstemp(dir=os.path.dirname(epub))
    os.close(tmpfd)

    # create a temp copy of the archive without filename            
    with zipfile.ZipFile(epub, 'r') as zin:
        with zipfile.ZipFile(tmpname, 'w') as zout:
            zout.comment = zin.comment # preserve the comment
            for item in zin.infolist():
                if item.filename == OPF:
                    orig_opf = zin.read(item.filename).decode('utf8')
                elif item.filename == NCX:
                    orig_ncx = zin.read(item.filename).decode('utf8')
                else:
                    zout.writestr(item, zin.read(item.filename))

    new_opf = []
    for l in orig_opf.split('\n'):
        if l.find('title_page_xhtml') >= 0:
            continue
        if l.find('</guide>') >= 0:
            new_opf.append('    <reference type="text" title="Summa librorum" href="ch002.xhtml" />')
        new_opf.append(l)
    new_opf.append('')
    new_opf = '\n'.join(new_opf).encode('utf8')

    new_ncx = []
    skip = 0
    for l in orig_ncx.split('\n'):
        if skip > 0:
            skip -= 1
            continue
        if l.find('"navPoint-0"') >= 0:
            skip = 5
            continue
        new_ncx.append(l)
    new_ncx.append('')
    new_ncx = '\n'.join(new_ncx).encode('utf8')

    # replace with the temp archive
    os.remove(epub)
    os.rename(tmpname, epub)

    # now add filename with its new data
    with zipfile.ZipFile(epub, mode='a', compression=zipfile.ZIP_DEFLATED) as zf:
        zf.writestr(OPF, new_opf)
        zf.writestr(NCX, new_ncx)
    return new_ncx, orig_ncx


def make_epub():
    print('Making Markdown...')
    Popen([os.path.join(BASEDIR, 'Scripts/makemarkdown.pl'), BASEDIR]).communicate()
    DATA = os.path.join(BASEDIR, 'Scripts/data.txt')
    EPUB = os.path.join(BASEDIR, 'Website/htdocs/vulgata.epub')
    MDPATH = os.path.join(BASEDIR, 'Scripts/markdown-out/')
    FRONTPATH = os.path.join(BASEDIR, 'Scripts/markdown-front/')
    books = [l.split('/')[0] for l in codecs.open(DATA, 'rb', 'cp1252') if l.strip() and l[0] != '#']

    print('Building epub...')
    Popen(['pandoc', 
        '--template', FRONTPATH + 'epub_template.pandoc',
        '-S', '-o', EPUB,
        '--epub-cover-image', FRONTPATH + 'title.png',
        FRONTPATH + 'title.txt', FRONTPATH +
           'front.md'] +
          [f'{MDPATH}/{f}.md' for f in books]).communicate()
    patch_epub(EPUB)

    print('Building mobi...')
    Popen(['kindlegen', EPUB, '-c2', '-verbose', '-o', 'vulgata.mobi']).communicate()

    print('Cleaning up markdown files...')
    for f in books:
        os.unlink(f'{MDPATH}/{f}.md')
    os.rmdir(MDPATH)

def make_epub_chapter_list():
    # one-time run; output included in front.md
    DATA = os.path.join(BASEDIR, 'Scripts/data.txt')
    books = OD([l.split('/', 3)[::2] for l in codecs.open(DATA, 'rb', 'cp1252') if l.strip() and l[0] != '#'])
    num_chaps = [50, 40, 27, 36, 34, 24, 21, 4, 31, 24, 22, 25, 29, 36, 10, 13, 14, 16, 16, 42, 150, 31, 12, 8, 19, 51, 66, 52, 5, 6, 48, 14, 14, 3, 9, 1, 4, 7, 3, 3, 3, 2, 14, 4, 16, 15, 28, 16, 24, 21, 28, 16, 16, 13, 6, 6, 4, 4, 5, 3, 6, 4, 3, 1, 13, 5, 5, 3, 5, 1, 1, 1, 22]
    print('# Summa librorum\n')
    fnum = 8
    for ((bk, name)), nc in zip(books.items(), num_chaps):
        print(f'- [{name}](ch{fnum:03d}.xhtml#{name.replace(" ", "_")})')
        fnum += 1
    print('\n# Summa capitulorum\n')
    snum = 0
    fnum = 8
    secname = 'section'
    for ((bk, name)), nc in zip(books.items(), num_chaps):
        print(f'- **{name}**', end='')
        for c in range(1, nc+1):
            print(f' [{c}](ch{fnum:03d}.xhtml#{secname})', end='')
            snum += 1
            secname = f'section-{snum}'
        print()
        fnum += 1

def commit(updated_text):
    cwd = os.getcwd()
    if updated_text:
        os.chdir(os.path.join(BASEDIR, 'Text'))
        Popen(['git', 'add'] + glob('*.lat')).communicate()
        Popen(['git', 'commit', '-m',
               f'Clementine text update at {PUBDATE}']).communicate()
        Popen(['git', 'push', 'origin', 'master']).communicate()
    os.chdir(os.path.join(BASEDIR, 'Website'))
    for f in glob('htdocs/.*.swp') + glob('htdocs/.*.un~') + glob('htdocs/*~') + glob('htdocs/mbt-*'):
        print(f'Deleting {f}')
        os.unlink(f)
    Popen(['git', 'add', '--all']).communicate()
    Popen(['git', 'commit', '-m',
           f'{"Text" if updated_text else "Website"} update at {PUBDATE}']).communicate()
    Popen(['git', 'push', 'origin', 'master']).communicate()
    os.chdir(BASEDIR)
    Popen(['tar', 'jcf', 'web.tar.bz2', 'Website',
           '--mode=a+rX', '--exclude-vcs']).communicate()
    Popen(['scp', 'web.tar.bz2', 'littlem@web.sf.net:']).communicate()
    os.unlink('web.tar.bz2')
    os.chdir(cwd)
    print('Ready for\nssh -t littlem@shell.sf.net create\ng ; ( umask 000 ; cd .. ; tar pjxf ~/web.tar.bz2 --strip-components 1 ; )')


def main():
    updated_text = False
    print('** Remember to run MakeTextUpdate.exe on Windows!')
    x = input('Add entry to RSS feed? (Y/n or e to update epub only) ').strip()
    if x == 'n' or x == 'N':
        pass
    else:
        updated_text = True
        if x != 'e':
            edit_rss()

    print('Making HTML...')
    Popen([os.path.join(BASEDIR, 'Scripts/makehtml.pl'), BASEDIR]).communicate()

    print('Making PDF...')
    Popen([os.path.join(BASEDIR, 'Scripts/archive/makelatex.pl'), BASEDIR]).communicate()
    Popen([os.path.join(BASEDIR, 'Scripts/archive/makepdf.sh'), os.path.join(BASEDIR, 'Scripts/archive/latex')]).communicate()

    print('Making TXT...')
    for fin in glob(BASEDIR + '/Text/*.lat'):
        fout = os.path.join(
            BASEDIR,
            'Website/htdocs/txt',
            os.path.basename(fin).replace(
                '.lat',
                '.txt'))
        with open(fout, 'w') as fh:
            Popen(['sed', '-f', os.path.join(BASEDIR,
                                             'Scripts/maketxt.sed'), fin], stdout=fh).communicate()

    if updated_text:
        make_epub()

    x = input('Ready to git commit and push? (Y/n) ').strip()
    if x == 'n' or x == 'N':
        pass
    else:
        commit(updated_text)


if __name__ == '__main__':
    main()
